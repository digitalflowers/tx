import {addDecorator, configure} from '@storybook/react';
import {withOptions} from '@storybook/addon-options';
import {themes} from '@storybook/components';
import {withThemesProvider} from 'storybook-addon-styled-component-theme';
import theme from "../src/theme";


// styled components
addDecorator(withThemesProvider([theme]));

// Option defaults.
addDecorator(
  withOptions({
    theme: themes.normal,
  })
);

function loadStories() {
  require('../src/stories');
}

configure(loadStories, module);
