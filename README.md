#Tx App  

#### Install Packages
```ssh
npm install
```

#### Run (Web)
```ssh
npm run start
```

#### Storybook
```ssh
npm run storybook
```

#### Test
```ssh
npm run test
```     
