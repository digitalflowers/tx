/**
 * currencies info
 * @type {*[]}
 */
export const CURRENCIES = {
  USD: {
    sign: "$",
    name: "US Dollar"
  },
  SGD: {
    sign: "S$",
    name: "Singapore Dollar"
  },
  JPY: {
    sign: "¥",
    name: "Japanese Yen"
  },
  KRW: {
    sign: "₩",
    name: "Korean Won"
  },
  GBP: {
    sign: "£",
    name: "British Sterling Pound"
  },
  CNY: {
    sign: "C¥",
    name: "Chinese Yuan"
  }
};
