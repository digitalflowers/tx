import axios from "axios";
import {getEnv} from "./env";

export const etherscan = axios.create({
  "baseURL": getEnv("API_ETHERSCAN_PROXY") || getEnv("API_ETHERSCAN_BASE_URL"),
  "timeout": parseInt(getEnv("API_TIMEOUT")),
  "headers": {
    "X-Requested-With": "XMLHttpRequest"
  },
  "params": {
    "apiKey": getEnv("API_ETHERSCAN_KEY")
  }
});
etherscan.interceptors.response.use(response => {
  if (response && response.data && (response.data.status === "1" || response.data.id === 1)) {
    return Promise.resolve(response);
  }
  // Error
  const message = response && response.data && (response.data.message || response.data.result) ? response.data.message || response.data.result : "Ops! Something went wrong.";
  return Promise.reject(new Error(message));
});


export const cryptocompare = axios.create({
  "baseURL": getEnv("API_CRYPTO_COMPARE_PROXY") || getEnv("API_CRYPTO_COMPARE_BASE_URL"),
  "timeout": parseInt(getEnv("API_TIMEOUT")),
  "headers": {
    "X-Requested-With": "XMLHttpRequest"
  },
  "params": {
    "api_key": getEnv("API_CRYPTO_COMPARE_KEY")
  }
});
cryptocompare.interceptors.response.use(response => {
  if (response && response.data && !(response.data.Response && response.data.Response === "Error")) {
    return Promise.resolve(response);
  }
  // Error
  const message = response && response.data && response.data.Message ? response.data.Message : "Ops! Something went wrong.";
  return Promise.reject(new Error(message));
});

