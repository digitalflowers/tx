const proxy = require('proxy-middleware');
const {getEnv} = require("./lib/env");

module.exports = app => {
  // setup proxies
  app.use(getEnv("REACT_APP_API_ETHERSCAN_PROXY"), proxy(getEnv("API_ETHERSCAN_BASE_URL")));
  app.use(getEnv("REACT_APP_API_CRYPTO_COMPARE_PROXY"), proxy(getEnv("REACT_APP_API_CRYPTO_COMPARE_BASE_URL")));
};
