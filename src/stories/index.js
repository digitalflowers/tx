import React from 'react';
import styled from 'styled-components';
import {Box} from "@rebass/grid";
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import {Text} from "../components/styled/text/Text";
import {Error} from "../components/styled/text/Error";
import {Button} from "../components/styled/elements/Button";
import {Dropdown} from "../components/styled/elements/Dropdown";
import {CURRENCIES} from "../lib/constants";
import {Input} from "../components/styled/elements/Input";
import {Header} from "../components/styled/others/Header";
import {Preloader} from "../components/styled/others/Preloader";
import {TransactionSlot} from "../components/styled/others/TransactionSlot";
import {TransactionDetails} from "../components/styled/others/TransactionDetails";

const StoryPreview = styled(Box)`
  min-height: calc(100vh - 16px);
  font-family: 'Roboto', sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
`;

StoryPreview.defaultProps = {
  p: 4,
  bg: "background"
};

storiesOf('Text', module)
  .add('default', () => (
    <StoryPreview>
      <Text>Sample Text</Text>
    </StoryPreview>
  ));

storiesOf('Error', module)
  .add('default', () => (
    <StoryPreview>
      <Error title="Some Error"/>
    </StoryPreview>
  ))
  .add('with body', () => (
    <StoryPreview>
      <Error title="Some Error">
        An error occurred while doing something.
      </Error>
    </StoryPreview>
  ));

storiesOf('Button', module)
  .add('default', () => (
    <StoryPreview>
      <Button onClick={action("Clicked :)")}>
        Click Me
      </Button>
    </StoryPreview>
  ));

storiesOf('Dropdown', module)
  .add('default', () => (
    <StoryPreview>
      <Dropdown value={1} onChange={value => action(`${value} is Selected`)()} items={[{
        value: 1,
        text: "Option 1"
      }, {
        value: 2,
        text: "Option 2"
      }, {
        value: 3,
        text: "Option 3"
      }]}/>
    </StoryPreview>
  ))
  .add('currencies', () => (
    <StoryPreview>
      <Dropdown value={"USD"}
                items={Object.keys(CURRENCIES).map(key => ({
                  value: key,
                  text: `${CURRENCIES[key].name} (${CURRENCIES[key].sign})`
                }))}
                onChange={value => action(`${value} is Selected`)()}/>
    </StoryPreview>
  ));

storiesOf('Input', module)
  .add('default', () => (
    <StoryPreview>
      <Input type={"text"}/>
    </StoryPreview>
  ));

storiesOf('Header', module)
  .add('default', () => (
    <StoryPreview p={0}>
      <Header title={"Tx App"} link={""}/>
    </StoryPreview>
  ))
  .add('with body', () => (
    <StoryPreview p={0}>
      <Header title={"Tx App"} link={""}>
        Welcome to Tx ETh Explorer
      </Header>
    </StoryPreview>
  ))
  .add('size after scroll', () => (
    <StoryPreview p={0}>
      <Header title={"Tx App"} isMin={true} link={""}>
        Welcome to Tx ETh Explorer
      </Header>
    </StoryPreview>
  ));


storiesOf('Preloader', module)
  .add('default', () => (
    <StoryPreview>
      <Preloader/>
    </StoryPreview>
  ))
  .add('larger x4', () => (
    <StoryPreview>
      <Preloader size={4}/>
    </StoryPreview>
  ))
  .add('custom color', () => (
    <StoryPreview>
      <Preloader size={4} color="lightblue"/>
    </StoryPreview>
  ));

storiesOf('Transaction', module)
  .add('slot (sent)', () => (
    <StoryPreview>
      <TransactionSlot onSelect={hash => action(`Transaction ${hash} Clicked :)`)()}
                       address="0xea674fdde714fd979de3edf0f56aa9716b898ec8"
                       transaction={{
                         "blockNumber": "7281507",
                         "timeStamp": "1551408797",
                         "hash": "0xeed2458cc629ad83b6259df4ec489958ddc53a9668f091c3164fb1f8dd237b29",
                         "nonce": "19119046",
                         "blockHash": "0x6469260eff884dfc49ca57cdbc7d46174ce82e1415772806a378e81f0aa23ade",
                         "transactionIndex": "5",
                         "from": "0xea674fdde714fd979de3edf0f56aa9716b898ec8",
                         "to": "0xb112288370183dafe916812fd5e0b89193256f8a",
                         "value": "50024217678360992",
                         "gas": "50000",
                         "gasPrice": "1000000000",
                         "isError": "0",
                         "txreceipt_status": "1",
                         "input": "0x",
                         "contractAddress": "",
                         "cumulativeGasUsed": "126102",
                         "gasUsed": "21051",
                         "confirmations": "2"
                       }}/>
    </StoryPreview>
  ))
  .add('slot (received)', () => (
    <StoryPreview>
      <TransactionSlot onSelect={hash => action(`Transaction ${hash} Clicked :)`)()}
                       address="0xb112288370183dafe916812fd5e0b89193256f8a"
                       transaction={{
                         "blockNumber": "7281507",
                         "timeStamp": "1551408797",
                         "hash": "0xeed2458cc629ad83b6259df4ec489958ddc53a9668f091c3164fb1f8dd237b29",
                         "nonce": "19119046",
                         "blockHash": "0x6469260eff884dfc49ca57cdbc7d46174ce82e1415772806a378e81f0aa23ade",
                         "transactionIndex": "5",
                         "from": "0xea674fdde714fd979de3edf0f56aa9716b898ec8",
                         "to": "0xb112288370183dafe916812fd5e0b89193256f8a",
                         "value": "50024217678360992",
                         "gas": "50000",
                         "gasPrice": "1000000000",
                         "isError": "0",
                         "txreceipt_status": "1",
                         "input": "0x",
                         "contractAddress": "",
                         "cumulativeGasUsed": "126102",
                         "gasUsed": "21051",
                         "confirmations": "2"
                       }}/>
    </StoryPreview>
  ))
  .add('details (rate 136.7 USD)', () => (
    <StoryPreview>
      <TransactionDetails enableLinks={false}
                          address="0xb112288370183dafe916812fd5e0b89193256f8a"
                          rate={136.7}
                          currency={"USD"}
                          transaction={{
                            "blockNumber": "7281507",
                            "timeStamp": "1551408797",
                            "hash": "0xeed2458cc629ad83b6259df4ec489958ddc53a9668f091c3164fb1f8dd237b29",
                            "nonce": "19119046",
                            "blockHash": "0x6469260eff884dfc49ca57cdbc7d46174ce82e1415772806a378e81f0aa23ade",
                            "transactionIndex": "5",
                            "from": "0xea674fdde714fd979de3edf0f56aa9716b898ec8",
                            "to": "0xb112288370183dafe916812fd5e0b89193256f8a",
                            "value": "50024217678360992",
                            "gas": "50000",
                            "gasPrice": "1000000000",
                            "isError": "0",
                            "txreceipt_status": "1",
                            "input": "0x",
                            "contractAddress": "",
                            "cumulativeGasUsed": "126102",
                            "gasUsed": "21051",
                            "confirmations": "2"
                          }}/>
    </StoryPreview>
  ));
