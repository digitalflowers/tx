import {all, call, put, takeLatest} from 'redux-saga/effects';
import * as api from './api';
import accountActionTypes from "./account/actionTypes";
import * as accountActions from "./account/actions";
import transactionsActionTypes from "./transactions/actionTypes";
import * as transactionsActions from "./transactions/actions";


/**
 * fetch account saga
 * @param action
 * @return {IterableIterator<*>}
 */
function* fetchAccountSaga(action) {
  try {
    const {address} = action.payload;
    // get balance and exchange rate
    const [{rates}, {balance}] = yield all([
      call(api.fetchRates),
      call(api.fetchBalance, {address})
    ]);
    yield put(accountActions.fetchAccountSuccess({balance, rates}));
    // get transactions
    yield put(transactionsActions.fetchTransactions({address}));
  } catch (error) {
    yield put(accountActions.fetchAccountFailed({error}));
  }
}

/**
 * fetch transactions saga
 * @param action
 * @return {IterableIterator<*>}
 */
function* fetchTransactionsSaga(action) {
  try {
    const {page, sort, offset, address} = action.payload;
    const {transactions} = yield call(api.fetchTransactions, {address, page, sort, offset});
    yield put(transactionsActions.fetchTransactionsSuccess({transactions}));
  } catch (error) {
    yield put(transactionsActions.fetchTransactionsFailed({error}));
  }
}

function* rootSaga() {
  // get only latest => disable concurrent
  yield takeLatest(accountActionTypes.FETCH_ACCOUNT, fetchAccountSaga);
  // get only latest => disable concurrent
  yield takeLatest(transactionsActionTypes.FETCH_TRANSACTIONS, fetchTransactionsSaga);
}

export default rootSaga;
