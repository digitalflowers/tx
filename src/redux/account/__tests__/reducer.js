import reducer from "../reducer";
import initial from "../initial";
import {fetchAccount, fetchAccountFailed, fetchAccountSuccess, setCurrency} from "../actions";
import {fetchBalance, fetchRates} from "../../__mocks__/api";


describe('account reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {}))
      .toEqual(initial)
  });

  it('should handle fetchAccount => fetchAccountSuccess', async () => {
    const address = "0x37df0d9beccd412951500f0e33f0c3721fb4dc31";
    const fetchAccountState = initial
      .set("address", address)
      .set("pending", true)
      .set("error", null);

    expect(reducer(undefined, fetchAccount({address})))
      .toEqual(fetchAccountState);

    const {balance} = await fetchBalance({address});
    const {rates} = await fetchRates();

    expect(reducer(fetchAccountState, fetchAccountSuccess({balance, rates})))
      .toEqual(fetchAccountState
        .set("balance", balance)
        .set("rates", rates)
        .set("pending", false)
      )
  });

  it('should handle fetchAccount => fetchAccountFailed', () => {
    const address = "0x37df0d9beccd412951500f0e33f0c3721fb4dc31";
    const fetchAccountState = initial
      .set("address", address)
      .set("pending", true)
      .set("error", null);

    expect(reducer(undefined, fetchAccount({address})))
      .toEqual(fetchAccountState);

    const error = new Error("some error");

    expect(reducer(fetchAccountState, fetchAccountFailed({error})))
      .toEqual(fetchAccountState
        .set("error", error)
        .set("pending", false)
      )
  });

  it('should handle setCurrency', () => {
    expect(reducer(initial, setCurrency({currency: "JPY"})))
      .toEqual(initial.set("currency", "JPY"));

    expect(reducer(initial, setCurrency({currency: "KKK"})))
      .toEqual(initial.set("currency", "USD"));
  });

});
