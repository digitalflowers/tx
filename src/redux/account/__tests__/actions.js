import actionTypes from "../actionTypes";
import {fetchAccount, fetchAccountFailed, fetchAccountSuccess, setCurrency} from "../actions";
import {fetchBalance, fetchRates} from "../../__mocks__/api";

describe('account actions', () => {
  it('should create action to fetchAccount', () => {
    const address = "0x37df0d9beccd412951500f0e33f0c3721fb4dc31";
    expect(fetchAccount({address}))
      .toEqual({
        type: actionTypes.FETCH_ACCOUNT,
        payload: {address}
      })
  });

  it('should create action to fetchAccountSuccess', async () => {
    const {balance} = await fetchBalance({address: "0x37df0d9beccd412951500f0e33f0c3721fb4dc31"});
    const {rates} = await fetchRates();
    expect(fetchAccountSuccess({balance, rates})).toEqual({
      type: actionTypes.FETCH_ACCOUNT_SUCCESS,
      payload: {balance, rates}
    })
  });

  it('should create action to fetchAccountFailed', () => {
    const error = new Error("some error");
    expect(fetchAccountFailed({error}))
      .toEqual({
        type: actionTypes.FETCH_ACCOUNT_FAILED,
        payload: {error}
      })
  });

  it('should create action to setCurrency', () => {
    const currency = "KRW";
    expect(setCurrency({currency}))
      .toEqual({
        type: actionTypes.SET_CURRENCY,
        payload: {currency}
      })
  });

});
