import initialState from "./initial";
import actionTypes from "./actionTypes";

export default (state = initialState, action) => {

  switch (action.type) {

    case actionTypes.FETCH_ACCOUNT:
      const {address} = action.payload;
      return state
        .set("pending", true)
        .set("error", null)
        .set("address", address)
        .set("balance", initialState.balance);

    case actionTypes.FETCH_ACCOUNT_SUCCESS:
      const {balance, rates} = action.payload;
      return state
        .set("pending", false)
        .set("rates", rates)
        .set("balance", balance);

    case actionTypes.FETCH_ACCOUNT_FAILED:
      return state
        .set("pending", false)
        .set("error", action.payload.error);

    case actionTypes.SET_CURRENCY:
      const {currency} = action.payload;
      return state.rates.has(currency) ? state
        .set("currency", action.payload.currency) : state;

    default:
      return state;
  }
};
