import actionTypes from "./actionTypes";

/**
 * fetch account
 * @param address
 * @return {{type: null, payload: {address: *}}}
 */
export const fetchAccount = ({address}) => ({
  type: actionTypes.FETCH_ACCOUNT,
  payload: {
    address
  }
});
/**
 * fetch account success
 * @param balance
 * @param rates
 * @return {{type: null, payload: {balance: *}}}
 */
export const fetchAccountSuccess = ({balance, rates}) => ({
  type: actionTypes.FETCH_ACCOUNT_SUCCESS,
  payload: {
    balance,
    rates
  }
});

/**
 * fetch account failed
 * @param error
 * @return {{type: null, payload: {error: *}}}
 */
export const fetchAccountFailed = ({error}) => ({
  type: actionTypes.FETCH_ACCOUNT_FAILED,
  payload: {
    error
  }
});

/**
 * set currency
 * @param currency
 * @return {{type: null, payload: {currency: *}}}
 */
export const setCurrency = ({currency = "USD"}) => ({
  type: actionTypes.SET_CURRENCY,
  payload: {
    currency
  }
});
