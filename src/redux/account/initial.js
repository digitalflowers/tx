import {recordFromJS} from "../../lib/immutable";

export default recordFromJS({
  address: "",
  balance: 0,
  currency: "USD",
  rates: {
    USD: 0,
    SGD: 0,
    JPY: 0,
    KRW: 0,
    GBP: 0,
    CNY: 0
  },
  pending: false,
  error: null
});
