import {reaction} from "dacho";

let types = {
  FETCH_ACCOUNT: null,
  FETCH_ACCOUNT_SUCCESS: null,
  FETCH_ACCOUNT_FAILED: null,
  SET_CURRENCY: null,
};

types = reaction(types, "ACCOUNT/");

export default types;
