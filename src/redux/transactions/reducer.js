import initialState from "./initial";
import actionTypes from "./actionTypes";

export default (state = initialState, action) => {

  switch (action.type) {

    case actionTypes.FETCH_TRANSACTIONS:
      const {page, sort, offset} = action.payload;
      return state
        .set("pending", true)
        .set("error", null)
        .set("page", page)
        .set("sort", sort)
        .set("offset", offset)
        .set("transactions", page === 1 ? initialState.transactions : state.transactions);

    case actionTypes.FETCH_TRANSACTIONS_SUCCESS:
      const {transactions} = action.payload;
      return state
        .set("pending", false)
        .set("transactions", state.transactions
          .concat(transactions)
          // remove duplicate by hash
          .groupBy(x => x.hash)
          .map(x => x.first())
          .toList()
        );

    case actionTypes.FETCH_TRANSACTIONS_FAILED:
      return state
        .set("pending", false)
        .set("error", action.payload.error);

    default:
      return state;
  }
};
