import reducer from "../reducer";
import initial from "../initial";
import {fetchTransactions, fetchTransactionsFailed, fetchTransactionsSuccess} from "../actions";
import {fetchTransactions as fetchTransactionsApi} from "../../__mocks__/api";


describe('transactions reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {}))
      .toEqual(initial)
  });

  it('should handle fetchTransactions => fetchTransactionsSuccess', async () => {
    const address = "0x37df0d9beccd412951500f0e33f0c3721fb4dc31";
    const page = 1;
    const sort = "desc";
    const offset = 20;
    const fetchTransactionsState = initial
      .set("pending", true)
      .set("page", page)
      .set("sort", sort)
      .set("offset", offset);

    expect(reducer(undefined, fetchTransactions({address, page, offset, sort})))
      .toEqual(fetchTransactionsState);

    const {transactions} = await fetchTransactionsApi({address, page, offset, sort});

    expect(reducer(fetchTransactionsState, fetchTransactionsSuccess({transactions})))
      .toEqual(fetchTransactionsState
        .set("transactions", transactions)
        .set("pending", false)
      )
  });

  it('should handle fetchTransactions => fetchTransactionsFailed', () => {
    const page = 1;
    const sort = "desc";
    const offset = 20;
    const error = new Error("some error");

    const fetchTransactionsState = initial
      .set("pending", true)
      .set("page", page)
      .set("sort", sort)
      .set("offset", offset);

    expect(reducer(fetchTransactionsState, fetchTransactionsFailed({error})))
      .toEqual(fetchTransactionsState
        .set("error", error)
        .set("pending", false)
      )
  });

});
