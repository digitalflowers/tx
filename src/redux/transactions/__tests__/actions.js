import actionTypes from "../actionTypes";
import {fetchTransactions, fetchTransactionsFailed, fetchTransactionsSuccess} from "../actions";
import {fetchTransactions as fetchTransactionsApi} from "../../__mocks__/api";

describe('transactions actions', () => {
  it('should create action to fetchTransactions', () => {
    const address = "1234";
    const page = 4;
    const sort = "DESC";
    const offset = 20;
    expect(fetchTransactions({address, page, sort, offset}))
      .toEqual({
        type: actionTypes.FETCH_TRANSACTIONS,
        payload: {address, page, sort, offset}
      })
  });

  it('should create action to fetchTransactionsSuccess', async () => {
    const address = "0x37df0d9beccd412951500f0e33f0c3721fb4dc31";
    const page = 1;
    const sort = "desc";
    const offset = 20;
    const {transactions} = await fetchTransactionsApi({address, page, offset, sort});

    expect(fetchTransactionsSuccess({transactions}))
      .toEqual({
        type: actionTypes.FETCH_TRANSACTIONS_SUCCESS,
        payload: {transactions}
      })
  });

  it('should create action to fetchTransactionsFailed', () => {
    const error = new Error("some error");
    expect(fetchTransactionsFailed({error}))
      .toEqual({
        type: actionTypes.FETCH_TRANSACTIONS_FAILED,
        payload: {error}
      })
  });
});
