import {reaction} from "dacho";

let types = {
  FETCH_TRANSACTIONS: null,
  FETCH_TRANSACTIONS_SUCCESS: null,
  FETCH_TRANSACTIONS_FAILED: null
};

types = reaction(types, "TRANSACTIONS/");

export default types;
