import actionTypes from "./actionTypes";

/**
 * fetch transactions
 * @param manufacturer
 * @return {{type: String, payload: {address:*}}}
 */
export const fetchTransactions = ({address, page = 1, sort = "desc", offset = 20}) => ({
  type: actionTypes.FETCH_TRANSACTIONS,
  payload: {
    address,
    page,
    sort,
    offset
  }
});

/**
 * set transactions info
 * @param transactions
 * @return {{type: null, payload: {transactions: *}}}
 */
export const fetchTransactionsSuccess = ({transactions}) => ({
  type: actionTypes.FETCH_TRANSACTIONS_SUCCESS,
  payload: {
    transactions
  }
});

/**
 * fetch transactions failed
 * @param error
 * @return {{type: String, payload: {error: *}}}
 */
export const fetchTransactionsFailed = ({error}) => ({
  type: actionTypes.FETCH_TRANSACTIONS_FAILED,
  payload: {
    error
  }
});
