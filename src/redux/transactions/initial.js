import {recordFromJS} from "../../lib/immutable";

export default recordFromJS({
  transactions: [],
  page: 1,
  offset: 20,
  sort: "desc",
  pending: false,
  error: null
});
