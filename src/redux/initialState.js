import account from "./account/initial";
import transactions from "./transactions/initial";

export default {
  account,
  transactions
}
