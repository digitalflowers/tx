import account from "./account/reducer";
import transactions from "./transactions/reducer";

/**
 * shared reducers
 */
export default {
  account,
  transactions
};
