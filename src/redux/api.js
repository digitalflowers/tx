import {cryptocompare, etherscan} from "../lib/api";
import {recordFromJS} from "../lib/immutable";
import {CURRENCIES} from "../lib/constants";

/**
 * fetch rates
 * @return {*}
 */
export const fetchRates = () => cryptocompare
  .get("/price", {
    params: {
      fsym: "ETH",
      tsyms: Object.keys(CURRENCIES).join(",")
    }
  })
  .then(({data}) => recordFromJS({rates: data}));

/**
 * fetch balance
 * @param address
 * @return {*}
 */
export const fetchBalance = ({address}) => etherscan
  .get("/", {
    params: {
      module: "account",
      action: "balance",
      address
    }
  })
  .then(({data: {result}}) => recordFromJS({balance: result}));

/**
 * fetch transactions
 * @param address
 * @param page
 * @param offset
 * @param sort
 * @return {*}
 */
export const fetchTransactions = ({address, page = 1, offset = 20, sort = "desc"}) => etherscan
  .get("/", {
    params: {
      module: "account",
      action: "txlist",
      address,
      sort,
      page,
      offset
    }
  })
  .then(({data: {result}}) => recordFromJS({transactions: result}));
