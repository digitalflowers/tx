import {recordFromJS} from "../../lib/immutable";
import balance from "./etherscan/balance";
import transactions from "./etherscan/transactions";
import price from "./cryptocompare/price";

/**
 * fetch rates mock
 * @return {Promise<any>}
 */
export const fetchRates = () => {
  const result = recordFromJS({rates: price});
  return Promise.resolve(result);
};

/**
 * fetch balance mock
 * @param address
 * @return {Promise<any>}
 */
export const fetchBalance = ({address}) => {
  const result = recordFromJS({balance: balance.result});
  return Promise.resolve(result);
};


/**
 * fetch transactions mock
 * @param address
 * @param page
 * @param offset
 * @param sort
 * @return {Promise<any>}
 */
export const fetchTransactions = ({address, page = 1, offset = 20, sort = "desc"}) => {
  const result = recordFromJS({transactions: transactions.result});
  return Promise.resolve(result);
};
