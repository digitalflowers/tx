export default {
  name: "Default",
  // font family
  font_family: {
    default: "'Roboto', sans-serif"
  },
  // font size
  font_size: [
    "8px",
    "12px",
    "14px",
    "18px",
    "32px"
  ],
  font_weight: {
    bold: "700",
    normal: "400",
    light: "300",
  },
  // colors
  colors: {
    background: "#1F1F1F",
    slot: "#353535",
    slot_active: "#2e2e2e",
    text: "#F7F7F7",
    text_datetime: "#A9A9A9",
    separator: "#4A4A4A",
    preloader: "#F7F7F7",
    button: "transparent",
    button_text: "#F7F7F7",
    input: "#353535",
    input_text: "#F7F7F7",
    input_border: "#F7F7F7",
    coin: "#BAA8A8"
  },
  // spacing
  space: [
    0,
    4,
    8,
    16,
    32,
    64,
    128,
    256,
    512
  ],
  // media query break point
  breakpoints: [
    '992px',
    '1200px'
  ]
};
