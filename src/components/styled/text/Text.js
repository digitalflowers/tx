import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * text
 */
export const Text = styled.p`
  margin: 0;
  transition: font-size .5s ease;
  line-height: ${props => props.lineHeight || 1.8};
  font-size: ${props => props.theme.font_size[props.fontSize] || props.fontSize || props.theme.font_size[2]};
  font-weight: ${props => props.theme.font_weight[props.fontWeight] || props.fontWeight || props.theme.font_weight.normal};
  text-align: ${props => props.textAlign || "left"};
  color: ${props => props.theme.colors[props.color] || props.color || props.theme.colors.text};
  display: ${props => props.display};
  width: ${props => props.width || "auto"};
`;

Text.propTypes = {
  textAlign: PropTypes.oneOf(["left", "right", "center"]),
  color: PropTypes.any,
  fontSize: PropTypes.any,
  lineHeight: PropTypes.any,
  fontWeight: PropTypes.any,
  display: PropTypes.any,
  width: PropTypes.any,
};
