import React from "react";
import PropTypes from "prop-types";
import {Box} from "@rebass/grid";
import {Text} from "./Text";

export const Error = ({title, children, ...rest}) => (
  <Box>
    <Text textAlign="center" fontSize={3}>
      <i className="fa fa-exclamation-triangle"/> {title}
    </Text>
    <Box css={{
      textOverflow: "ellipsis",
      whiteSpace: "pre-wrap",
      overflow: "hidden"
    }}>
      <Text textAlign="center" display="inline-block" width="100%">
        {children}
      </Text>
    </Box>
  </Box>
);
Error.propTypes = {
  title: PropTypes.string
};
