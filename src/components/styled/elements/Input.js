import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * button
 */
export const Input = styled.input`
  line-height: 32px;
  display: inline-block;
  text-decoration: none;
  transition: 0.2s background-color;
  border: 1px solid ${props => props.theme.colors[props.borderColor] || props.borderColor || props.theme.colors.input_border};
  font-weight: ${props => props.theme.font_weight.bold};
  color: ${props => props.theme.colors[props.color] || props.color || props.theme.colors.input_text};
  font-size: ${props => props.theme.font_size[props.fontSize] || props.fontSize || props.theme.font_size[2]};
  background-color: ${props => props.theme.colors[props.backgroundColor] || props.backgroundColor || props.theme.colors.input};
  width: ${props => props.width || "400px"};
  text-indent: ${props => props.theme.space[2] + "px"};
  @media (max-width: ${props => props.theme.breakpoints[0]}) {
    border-left-width: 0;
    border-right-width: 0;
    width: ${props => props.width || "100%"};
  }
`;

Input.propTypes = {
  color: PropTypes.any,
  fontSize: PropTypes.any,
  backgroundColor: PropTypes.any,
  borderColor: PropTypes.any,
  width: PropTypes.any,
};
