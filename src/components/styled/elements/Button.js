import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * button
 */
export const Button = styled.button`
  text-align: center;
  line-height: 32px;
  cursor: pointer;
  display: inline-block;
  text-decoration: none;
  transition: 0.2s opacity;
  border: none;
  font-weight: ${props => props.theme.font_weight[props.fontWeight] || props.fontWeight || props.theme.font_weight.bold};
  color: ${props => props.theme.colors[props.color] || props.color || props.theme.colors.button_text};
  font-size: ${props => props.theme.font_size[props.fontSize] || props.fontSize || props.theme.font_size[3]};
  background-color: ${props => props.theme.colors[props.backgroundColor] || props.backgroundColor || props.theme.colors.button};
  min-width: ${props => props.minWidth || "128px"};
  :hover, :active, :focus {
    text-decoration: none;
    opacity: 0.8;
  }
`;

Button.propTypes = {
  color: PropTypes.any,
  fontSize: PropTypes.any,
  backgroundColor: PropTypes.any,
  minWidth: PropTypes.any,
  fontWeight: PropTypes.any,
};


/**
 * button plain
 */
export const ButtonPlain = styled.button`
  cursor: pointer;
  text-decoration: none;
  transition: 0.2s opacity;
  border: none;
  padding: 0;
  margin: 0;
  width: ${props => props.width || "128px"};
  min-width: ${props => props.minWidth || "128px"};
  background-color: ${props => props.theme.colors[props.backgroundColor] || props.backgroundColor || props.theme.colors.button};
  &.active, :hover, :active, :focus {
    text-decoration: none;
    opacity: 0.8;
  }
`;

ButtonPlain.propTypes = {
  backgroundColor: PropTypes.any,
  width: PropTypes.any,
  minWidth: PropTypes.any,
};
