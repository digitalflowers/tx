import React from "react";
import PropTypes from "prop-types";
import {Box} from "@rebass/grid";
import {Text} from "../text/Text";
import {Separator} from "./Separator";
import {Link} from "react-router-dom";
import {ButtonPlain} from "../elements/Button";

/**
 * header
 */
export const Header = React.forwardRef(({title, as, isMin, link, children}, ref) => (
  <Box>
    <Box ref={ref}>
      <Box px={3} py={isMin ? 3 : 4} css={{
        transition: "padding .5s ease"
      }}>
        <ButtonPlain as={link ? Link : "span"} to={link}>
          <Text fontSize={isMin ? 3 : 4} fontWeight="bold" textAlign={"center"} as={"h1"}>
            {title}
          </Text>
        </ButtonPlain>
        <Text fontSize={isMin ? 2 : 3} textAlign={"center"} as={as}>
          {children}
        </Text>
      </Box>
    </Box>
    <Separator/>
  </Box>
));

Header.propTypes = {
  title: PropTypes.any,
  as: PropTypes.any,
  isMin: PropTypes.bool,
  link: PropTypes.any
};


Header.defaultProps = {
  isMin: false,
  link: "/"
};
