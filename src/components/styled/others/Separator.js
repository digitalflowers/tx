import styled from "styled-components";
import PropTypes from "prop-types";

/**
 * mini preloader
 */
export const Separator = styled.hr`
  height: 0;
  margin: 0;
  border-bottom: ${props => props.theme.space[props.size] || props.size || "1px"};
  border-style: solid;
  color: ${props => props.theme.colors[props.color] || props.color || props.theme.colors.separator};
`;

Separator.propTypes = {
  size: PropTypes.any,
  color: PropTypes.any,
};
