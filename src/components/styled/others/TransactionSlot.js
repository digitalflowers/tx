import React, {Component} from "react";
import moment from "moment";
import PropTypes from "prop-types";
import {Box, Flex} from "@rebass/grid";
import {Text} from "../text/Text";
import {Separator} from "./Separator";
import {ButtonPlain} from "../elements/Button";

/**
 * transaction slot
 */
export class TransactionSlot extends Component {
  toETH = value => value / 1e18;

  render() {
    const {onSelect, address, transaction, isLast, isSelected} = this.props;
    const {hash, timeStamp, from, to, value} = transaction || {};
    return (
      <Box>
        <ButtonPlain onClick={() => onSelect && onSelect(hash)} className={isSelected ? "active" : ""} width="100%">
          <Box bg="slot">
            <Flex alignItems="center">
              <Box bg='coin' width="50px" m={3} css={{
                borderRadius: "50%"
              }}>
                <Text fontSize={4} textAlign="center" lineHeight="50px">
                  <i className="fab fa-ethereum"/>
                </Text>
              </Box>
              <Box flex={1} m={2} css={{
                overflow: "hidden"
              }}>
                <Flex>
                  <Box flex={1} css={{
                    minWidth: "0",
                    overflow: "hidden",
                    whiteSpace: "pre-wrap",
                    textOverflow: "ellipsis",
                    textAlign: "left"
                  }}>
                    <Text fontSize={3} fontWeight="bold" display="inline-block">
                      {this.toETH(value)} ETH
                    </Text>
                  </Box>
                  <Box px={1}>
                    <Text fontSize={3} color="text_datetime">
                      {moment.unix(timeStamp).calendar(null, {
                        "sameDay": "[Today]",
                        "nextDay": "[Tomorrow]",
                        "nextWeek": "dddd",
                        "lastDay": "[Yesterday]",
                        "lastWeek": "[Last] dddd",
                        "sameElse": "ll"
                      })}
                    </Text>
                  </Box>
                  <Box px={1}>
                    <Text fontSize={3} color="text_datetime">
                      <i className="fa fa-chevron-right"/>
                    </Text>
                  </Box>
                </Flex>
                <Box>
                  {from && from.toLowerCase() !== address ? (
                    <Box css={{
                      minWidth: "0",
                      overflow: "hidden",
                      whiteSpace: "nowrap",
                      textOverflow: "ellipsis",
                      textAlign: "left"
                    }}>
                      <Text>
                        RECEIVED FROM
                      </Text>
                      <Text fontSize={3} display="inline-block" width="100%">
                        {from}
                      </Text>
                    </Box>
                  ) : null}
                  {to && to.toLowerCase() !== address ? (
                    <Box>
                      <Text>
                        SENT TO
                      </Text>
                      <Box css={{
                        minWidth: "0",
                        overflow: "hidden",
                        whiteSpace: "nowrap",
                        textOverflow: "ellipsis",
                        textAlign: "left"
                      }}>
                        <Text fontSize={3} display="inline-block">
                          {to}
                        </Text>
                      </Box>
                    </Box>
                  ) : null}
                </Box>
              </Box>
            </Flex>
            <Box pl={isLast ? 0 : 3}>
              <Separator/>
            </Box>
          </Box>
        </ButtonPlain>
      </Box>
    );
  }
}

TransactionSlot.propTypes = {
  transaction: PropTypes.any,
  address: PropTypes.string,
  isLast: PropTypes.bool,
  onSelect: PropTypes.func,
  isSelected: PropTypes.bool
};
