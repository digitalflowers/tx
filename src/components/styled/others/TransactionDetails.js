import React, {Component} from "react";
import moment from "moment";
import PropTypes from "prop-types";
import {Box} from "@rebass/grid";
import {Link} from "react-router-dom";
import {Text} from "../text/Text";
import {Button} from "../elements/Button";
import {CURRENCIES} from "../../../lib/constants";

/**
 * transaction details
 */
export class TransactionDetails extends Component {
  toETH = value => value / 1e18;

  toCurrency = value => Math.floor(this.props.rate * (value / 1e18) * 1000) / 1000;

  render() {
    const {address, transaction, currency, enableLinks} = this.props;
    const {hash, timeStamp, from, to, value, txreceipt_status, gasUsed, gasPrice, blockHash} = transaction || {};
    const currencyInfo = CURRENCIES[currency];
    return (
      <Box css={{
        wordBreak: "break-all"
      }}>
        <Box>
          <Text fontSize={3} fontWeight="bold">
            Transaction ID
          </Text>
          <Text>
            {hash}
          </Text>
        </Box>
        <Box>
          <Text fontSize={3} fontWeight="bold">
            Type
          </Text>
          <Text>
            {from && from.toLowerCase() === address ? "Send" : "Receive"}
          </Text>
        </Box>
        <Box>
          <Text fontSize={3} fontWeight="bold">
            {from && from.toLowerCase() === address ? "Destination" : "Source"}
          </Text>
          <Text>
            {from && from.toLowerCase() === address ? (
              <Button as={enableLinks ? Link : "span"} to={`/account/${to}`} fontWeight="normal" fontSize={2}>
                {to}
              </Button>
            ) : (
              <Button as={enableLinks ? Link : "span"} to={`/account/${from}`} fontWeight="normal" fontSize={2}>
                {from}
              </Button>
            )}
          </Text>
        </Box>
        <Box>
          <Text fontSize={3} fontWeight="bold">
            Status
          </Text>
          <Text>
            {txreceipt_status === "1" ? "Success" : (
              txreceipt_status === "0" ? "Failed" : "Pending"
            )}
          </Text>
        </Box>
        <Box>
          <Text fontSize={3} fontWeight="bold">
            Amount
          </Text>
          <Text>
            {currencyInfo.sign} {this.toCurrency(value)} ({this.toETH(value)} ETH)
          </Text>
        </Box>
        <Box>
          <Text fontSize={3} fontWeight="bold">
            Transaction fees
          </Text>
          <Text>
            {currencyInfo.sign} {this.toCurrency(gasUsed * gasPrice)} ({this.toETH(gasUsed * gasPrice)} ETH)
          </Text>
        </Box>
        <Box>
          <Text fontSize={3} fontWeight="bold">
            Created
          </Text>
          <Text>
            {moment.unix(timeStamp).calendar(null, {
              "sameDay": "[Today] - ",
              "nextDay": "[Tomorrow] - ",
              "nextWeek": "dddd - ",
              "lastDay": "[Yesterday] - ",
              "lastWeek": "[Last] dddd - ",
              "sameElse": " "
            })} {moment.unix(timeStamp).format("lll")}
          </Text>
        </Box>
        <Box>
          <Text fontSize={3} fontWeight="bold">
            Block Confirmations
          </Text>
          <Text>
            {blockHash}
          </Text>
        </Box>
      </Box>
    );
  }
}

TransactionDetails.propTypes = {
  transaction: PropTypes.any,
  address: PropTypes.string,
  currency: PropTypes.string,
  rate: PropTypes.number,
  enableLinks: PropTypes.bool
};

TransactionDetails.defaultProps = {
  enableLinks: true
};
