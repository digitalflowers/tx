import React, {Component} from "react"
import {connect} from "react-redux";
import {push} from "connected-react-router";
import {Box} from "@rebass/grid";
import {Button} from "../styled/elements/Button";
import {Input} from "../styled/elements/Input";
import {Header} from "../styled/others/Header";

class IndexRoute extends Component {
  state = {
    address: ''
  };

  componentWillMount() {
    this.setState({
      address: this.props.address
    })
  }

  handleChange = event => {
    this.setState({address: event.target.value.trim()});
  };

  handleSubmit = event => {
    this.state.address && this.props.dispatch(push(`/account/${this.state.address.toLowerCase()}`));
    event.preventDefault();
    return false;
  };

  render() {
    const {address} = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <Box css={{
          minHeight: "100vh",
          overflow: "hidden"
        }}>
          <Header title={"My Portfolio"}>
            Enter an Ethereum address to get started
          </Header>
          <Box css={{
            textAlign: "center"
          }}>
            <Box mx="auto" my={3} width={["100%", "auto"]}>
              <Input type="text" value={address} onChange={this.handleChange} required/>
            </Box>
            <Box>
              <Button type="submit">
                Continue
              </Button>
            </Box>
          </Box>
        </Box>
      </form>
    );
  }
}

export default connect(({account: {address}}) => ({
  address
}))(IndexRoute);
