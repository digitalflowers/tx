import React, {Component} from "react";
import {Link} from "react-router-dom";
import {Box, Flex} from "@rebass/grid";
import {Text} from "../styled/text/Text";
import {Button} from "../styled/elements/Button";

const errorTexts = [
  // eslint-disable-next-line
  "(·.·) ",
  // eslint-disable-next-line
  "(˚Δ˚) ",
  // eslint-disable-next-line
  "(·_·) ",
  // eslint-disable-next-line
  "(^_^)",
  // eslint-disable-next-line
  " (>_<) ",
  // eslint-disable-next-line
  "(o^^)o",
  // eslint-disable-next-line
  " (;-;) ",
  // eslint-disable-next-line
  "(≥o≤) ",
  // eslint-disable-next-line
  "(^-^*) ",
  // eslint-disable-next-line
  "(='X'=)"
];

class NotFoundRoute extends Component {
  render() {
    const errorText = errorTexts[Math.floor(Math.random() * errorTexts.length - 1)];
    return (
      <Flex alignItems="center" justifyContent="center" css={{
        minHeight: "100vh",
        overflow: "hidden"
      }}>
        <Box p={2}>
          <Text fontSize={4} fontWeight="bold" textAlign={"center"} as={"h1"}>
            Page Not Found {errorText}
          </Text>
          <Text fontSize={3} textAlign={"center"}>
            Sorry, the page you are looking for does not exist.<br/>
            You can always go back to the <Button as={Link} to="/" minWidth="auto">homepage</Button>.
          </Text>
        </Box>
      </Flex>
    );
  }
}

export default NotFoundRoute;
