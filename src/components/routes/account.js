import React, {Component} from "react";
import {connect} from "react-redux";
import {Box, Flex} from "@rebass/grid";
import {Link} from "react-router-dom";
import Measure from "react-measure";
import throttle from "lodash.throttle";
import {Header} from "../styled/others/Header";
import {Text} from "../styled/text/Text";
import {Preloader} from "../styled/others/Preloader";
import {Error} from "../styled/text/Error";
import {Button} from "../styled/elements/Button";
import {TransactionSlot} from "../styled/others/TransactionSlot";
import {Dropdown} from "../styled/elements/Dropdown";
import {TransactionDetails} from "../styled/others/TransactionDetails";
import {CURRENCIES} from "../../lib/constants";
import {fetchAccount, setCurrency} from "../../redux/account/actions";
import {fetchTransactions} from "../../redux/transactions/actions";

class AccountRoute extends Component {

  state = {
    selectedTransaction: null,
    headerHeight: 0,
    minHeader: false
  };

  componentWillMount() {
    const {match} = this.props;
    if (match && match.params && match.params.address) {
      this.fetchAccount({address: match.params.address});
    }
    // sticky header
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    // sticky header
    window.removeEventListener("scroll", this.handleScroll);
  }

  componentDidUpdate({match: prevMatch}) {
    const {match} = this.props;
    if (match && match.params && match.params.address && (!prevMatch || !prevMatch.params || prevMatch.params.address !== match.params.address)) {
      this.removeSelectedTransaction();
      this.fetchAccount({address: match.params.address});
    }
  }

  handleScroll = throttle(() => {
    const {pending, error, page, match} = this.props;
    const {headerHeight, minHeader} = this.state;
    const newMinHeader = window.pageYOffset > headerHeight * 0.75;
    if (newMinHeader !== minHeader) {
      this.setState({minHeader: newMinHeader});
    }
    // check if scroll reach last
    if (window.pageYOffset >= document.body.scrollHeight - window.innerHeight - 200) {
      if (!pending && !error && match && match.params && match.params.address) {
        this.fetchTransactions({address: match.params.address, page: page + 1})
      }
    }
  }, 100);

  fetchAccount = filter => {
    const {dispatch} = this.props;
    dispatch(fetchAccount(filter));
  };

  fetchTransactions = filter => {
    const {dispatch} = this.props;
    dispatch(fetchTransactions(filter));
  };

  setCurrency = currency => {
    const {dispatch} = this.props;
    dispatch(setCurrency({currency}));
  };

  selectTransaction = selectedTransaction => {
    this.setState({selectedTransaction});
  };

  removeSelectedTransaction = () => this.setState({selectedTransaction: null});

  toETH = value => value / 1e18;

  toCurrency = value => Math.floor(this.props.rates[this.props.currency] * (value / 1e18) * 1000) / 1000;

  render() {
    const {transactions, balance, currency, accountPending, accountError, rates, match, pending, error, page} = this.props;
    const {selectedTransaction, headerHeight, minHeader} = this.state;
    const address = match && match.params && match.params.address ? match.params.address.toLowerCase() : "";
    const currencyInfo = CURRENCIES[currency];
    return (
      <Box css={{
        minHeight: "100vh",
        overflow: "hidden",
        paddingTop: `${headerHeight}px`,
        transition: "padding .5s ease"
      }}>
        <Box bg="background" css={{
          width: "100%",
          top: 0,
          zIndex: 99,
          position: "fixed"
        }}>
          <Measure bounds onResize={contentRect => {
            this.setState({headerHeight: contentRect.bounds.height})
          }}>
            {({measureRef}) => (
              <Header ref={measureRef}
                      title={"My Portfolio"}
                      as="div"
                      isMin={minHeader}>
                {!accountPending && !accountError ? (
                  <Box>
                    <Dropdown text={`${currencyInfo.sign} ${this.toCurrency(balance)}`}
                              items={Object.keys(CURRENCIES).map(key => ({
                                value: key,
                                text: `${CURRENCIES[key].name} (${CURRENCIES[key].sign})`
                              }))}
                              onChange={key => this.setCurrency(key)}/> <Box as="span"
                                                                             css={{whiteSpace: "nowrap"}}>({this.toETH(balance)} ETH)</Box>
                  </Box>
                ) : ""}
              </Header>
            )}
          </Measure>
        </Box>
        <Flex>
          <Box flex={[selectedTransaction ? 0 : 1, 1]} css={{
            transition: "0.6s flex",
            overflow: "hidden"
          }}>
            {accountPending ? (
              <Box p={5}>
                <Text textAlign="center">
                  <Preloader size={4}/>
                </Text>
              </Box>
            ) : accountError ? (
              <Box p={4}>
                <Error title={accountError.message}>
                  An error occurred while fetching `<Text as="span" fontWeight="bold">{address}</Text>` data.
                </Error>
                <Box p={3}>
                  <Flex justifyContent="center">
                    <Button as={Link} to="/">
                      Try Another Address
                    </Button>
                    <Button onClick={() => this.fetchAccount({address})}>
                      Retry
                    </Button>
                  </Flex>
                </Box>
              </Box>
            ) : (
              <Box>
                <Box>
                  {transactions
                    .toJS()
                    .map((transaction, index) => transaction ? (
                        <TransactionSlot key={transaction.hash}
                                         address={address}
                                         transaction={transaction}
                                         isLast={index >= transactions.size - 1}
                                         onSelect={() => this.selectTransaction(transaction)}
                                         isSelected={selectedTransaction && selectedTransaction.hash === transaction.hash}/>
                      ) : null
                    )}
                </Box>
                {pending ? (
                  <Box p={5}>
                    <Text textAlign="center">
                      <Preloader size={4}/>
                    </Text>
                  </Box>
                ) : error ? (
                  <Box p={4}>
                    <Error title={error.message}>
                      {error.message ? "" : (
                        <Box as="span">
                          An error occurred while fetching `<Text as="span" fontWeight="bold">{address}</Text>`
                          transactions.
                        </Box>
                      )}
                    </Error>
                    <Box p={3}>
                      <Text as="div" textAlign="center">
                        <Button onClick={() => this.fetchTransactions({address, page})}>
                          Retry
                        </Button>
                      </Text>
                    </Box>
                  </Box>
                ) : null}
              </Box>
            )}
          </Box>
          <Box width={[0, selectedTransaction ? "2px" : 0]} bg="separator"/>
          {!accountPending && !accountError ? (
            <Box flex={selectedTransaction ? 1 : 0} bg="slot" css={{
              transition: "0.6s flex",
              overflow: "hidden",
              opacity: 0.8
            }}>
              {selectedTransaction ? (
                <Box py={2} px={3} width={["100%", "50%"]} bg="slot" css={{
                  position: "fixed",
                  overflow: "hidden"
                }}>
                  <Box css={{
                    position: "absolute",
                    right: "0",
                    top: "0"
                  }}>
                    <Button minWidth="36px" onClick={() => this.removeSelectedTransaction()}>
                      <Text fontSize="24px">
                        <i className="fas fa-times"/>
                      </Text>
                    </Button>
                  </Box>
                  <TransactionDetails transaction={selectedTransaction}
                                      currency={currency}
                                      address={address}
                                      rate={rates[currency]}/>
                </Box>
              ) : null}
            </Box>
          ) : null}
        </Flex>
      </Box>
    );
  }
}

export default connect(({account: {balance, rates, currency, pending: accountPending, error: accountError}, transactions: {transactions, pending, error, page}}) => ({
  balance,
  transactions,
  rates,
  currency,
  page,
  accountPending,
  accountError,
  pending,
  error
}))(AccountRoute);
