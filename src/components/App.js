import React, {Component} from 'react';
import {Provider} from "react-redux";
import {ConnectedRouter} from "connected-react-router";
import {Route, Switch} from "react-router";
import {ThemeProvider} from "styled-components";
import {Box, Flex} from "@rebass/grid";
import configureWebStore, {history} from "../redux/configureWebStore";
import {Preloader} from "./styled/others/Preloader";
import {Text} from "./styled/text/Text";
import IndexRoute from "./routes";
import NotFoundRoute from "./routes/404";
import AccountRoute from "./routes/account";
import theme from "../theme";

class App extends Component {
  store = null;
  destroyed = false;

  state = {
    ready: false
  };

  componentDidMount() {
    // wait until app state dehydrate from local storage
    this.store = configureWebStore(() => !this.destroyed && this.setState({ready: true}));
  }

  componentWillUnmount() {
    this.destroyed = true;
  }

  render() {
    const {ready} = this.state;
    return (
      <ThemeProvider theme={theme}>
        {ready ? (
          <Provider store={this.store}>
            <ConnectedRouter history={history}
                             onLocationChanged={() => console.log("location changed")}>
              <Switch>
                <Route path="/" exact component={IndexRoute}/>
                <Route path="/account/:address" exact component={AccountRoute}/>
                <Route component={NotFoundRoute}/>
              </Switch>
            </ConnectedRouter>
          </Provider>
        ) : (
          <Flex alignItems="center" justifyContent="center" css={{
            minHeight: "100vh"
          }}>
            <Box flex={1}>
              <Text textAlign="center">
                <Preloader size={4}/>
              </Text>
            </Box>
          </Flex>
        )}
      </ThemeProvider>
    );
  }
}

export default App;
